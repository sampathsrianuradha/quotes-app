<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;


class GetQuotesTest extends TestCase
{
    /** @test */
    public function is_it_returns_cached_quotes()
    {
        Cache::put('cached_quotes', ['quote1', 'quote2'], now()->addMinutes(1));

        Session::put('authenticated', true);

        $response = $this->get('/api/quotes');

        // Assert the response contains the cached quotes
        $response->assertJson(['quote1', 'quote2']);

        Cache::forget('cached_quotes');
    }
}
