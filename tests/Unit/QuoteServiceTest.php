<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Quote;
use App\Services\QuoteService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class QuoteServiceTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function it_fetches_and_stores_quotes()
    {

        Config::set('quote.quotes_count', 5);
        Config::set('quote.api_url', 'https://api.kanye.rest');

        //new QuoteService instance
        $quoteService = new QuoteService();

        $quotes = $quoteService->fetchQuotes();

        //Assert count quotes were stored in the database
        $this->assertCount(5, Quote::all());

        //Assert fetched quotes are the same as stored quotes
        $this->assertEquals($quotes, Quote::pluck('quote')->toArray());
    }
}
