<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuoteController;
use App\Http\Controllers\ProtectedController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::post('/protected', [ProtectedController::class, 'authenticate'])->name('protected.authenticate');
Route::get('/protected', [ProtectedController::class, 'showForm'])->name('protected.entry');

Route::middleware(['protected'])->group(function () {
  
  Route::get('/', [QuoteController::class, 'index'])->name('index');

});