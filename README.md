# Amperative Quotes App

The Amperative Quotes App is a Laravel + Vuejs application that fetches quotes from the Kanye.rest API, stores unique quotes in the database, and provides an endpoint to retrieve cached quotes. The app also includes password protection.

## Features

- Fetches random quotes from the Kanye.rest API.
- Stores unique quotes in the database to avoid duplication.
- Provides a protected route to view cached quotes.
- Password protection for accessing certain routes.
- Periodically updates cached quotes from the API.

## Installation

Clone the repository:

   ```sh
   git clone https://gitlab.com/sampathsrianuradha/quotes-app
   cd quotes-app
   ```

Install dependencies:
  ```sh 
  quotes-app
  ```

Copy the .env.example file to .env:
  ```sh
  cp .env.example .env
  ```

Configure database connection in .env file and migrate
  ```sh
  php artisan migrate
  ```


Start the server:
  ```sh
  php artisan serve
  ```

Application can be access at http://localhost:8000

Set app password in .env file 


## Future improvements suggestions 

* fetchQuotes function can be optimize to handle to large amount of data.
* using Cron jobs API calls can be streamline.
* query filter option can be added to local API endpoint  
* Unit test can be write
* UI improvements 
