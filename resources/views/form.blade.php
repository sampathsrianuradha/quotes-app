@extends('app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">

    <div class="card">
      <h5 class="card-header">Protected Page</h5>
      <div class="card-body">
         
        @if(session('error'))
          <div class="alert alert-danger">
              {{ session('error') }}
          </div>
        @endif
        
        <form method="POST" action="{{ route('protected.authenticate') }}">
          @csrf
          <div class="mb-3">
            <label for="password" class="form-label">Enter Password:</label>
            <input type="password" name="password" class="form-control" required>
          </div>
          <button type="submit" class="btn btn-primary">Access</button>
        </form>
      </div>
      <div class="card-footer">
        
      </div>
    </div>
  
  </div>
  </div>
</div>
@endsection