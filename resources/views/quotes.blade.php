@extends('app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">

    <div class="card">
      <h5 class="card-header">Kenye's Quotes</h5>
      <div class="card-body">
        <ul class="list-group">
            
        </ul>
        
        <quotes-component></quotes-component>
       
      </div>
      <div class="card-footer">
        
      </div>
    </div>
  
  </div>
  </div>
</div>
@endsection