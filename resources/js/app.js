import './bootstrap';
import "bootstrap";

import { createApp } from 'vue';
import QuotesComponent from './components/QuotesComponent.vue';

const app = createApp({});

app.component("quotes-component", QuotesComponent);

const mountedApp = app.mount("#app");
