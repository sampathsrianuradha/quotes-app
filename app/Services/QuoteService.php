<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use App\Models\Quote;
use Illuminate\Support\Facades\Config;

class QuoteService
{
    public function fetchQuotes(): array
    {
        //get value from .env configuration file
        $count = Config::get('quote.quotes_count');
        $apiUrl = Config::get('quote.api_url');

        $quotes = [];
        //Store quotes from the database in to a array
        $quotesFromDB = Quote::pluck('quote')->toArray();

        // Fetch quotes until the desired count is reached
        while (count($quotes) < $count) {
            $response = Http::get($apiUrl);
            $text = $response->json()['quote'];
 
            //Check if the quote text is not in the database, then save it
            if (!in_array($text, $quotesFromDB)) {

                $quote = new Quote(['quote' => $text]);
                $quote->save();

                $quotes[] = $text;
                $quotesFromDB[] = $text;
            }
        }

        //Cache the fetched quotes for a duration
        Cache::put('cached_quotes', $quotes, now()->addMinutes(2));
        return $quotes;
    }
}
