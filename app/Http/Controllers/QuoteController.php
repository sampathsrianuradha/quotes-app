<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Services\QuoteService;
use Illuminate\Support\Facades\Cache;


class QuoteController extends Controller
{
    protected $quoteService;

    public function __construct(QuoteService $quoteService)
    {
        $this->quoteService = $quoteService;
    }
    
    public function index()
    {
      return view('quotes');
    }

  /**
   * Get quotes from cache or from quoteService
   * return JSON
   */
    public function getQuotes()
    {
      $quotes = [];
      //get quotes from the cache
      $quotes = Cache::get('cached_quotes', []);

      // If the cache is empty, fetch quotes using the quoteService
      if (empty($quotes)) {
        $quotes = $this->quoteService->fetchQuotes();
      }
      return response()->json($quotes);
    }
}
