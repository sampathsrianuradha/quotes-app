<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class ProtectedController extends Controller
{
    public function showForm()
    {
        return view('form');
    }

    public function authenticate(Request $request)
    {
        $providedPassword = $request->input('password');
        $correctPassword = config('quote.app_password');

        if ($providedPassword === $correctPassword) {
            Session::put('authenticated', true, now()->addMinutes(1));
            return redirect()->intended();
        } else {
            return redirect()->back()->with('error', 'Invalid password');
        }
    }
}
