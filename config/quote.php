<?php 
return [
    'quotes_count' => env('QUOTES_COUNT', 5),
    'api_url' => env('API_URL', 'https://api.kanye.rest'),
    'app_password' => env('APP_PASSWORD', ''),
];